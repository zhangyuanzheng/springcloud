package com.hz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;

@SpringBootApplication
@EnableHystrixDashboard //http://127.0.0.1:8080/hystrix 可视化项目监控注解  放入消费者端口号指定的项目监控地址 http://127.0.0.1:5557/hystrix.stream
public class MemberCloudDashboardApplication {

    public static void main(String[] args) {
        SpringApplication.run(MemberCloudDashboardApplication.class, args);
    }

}
