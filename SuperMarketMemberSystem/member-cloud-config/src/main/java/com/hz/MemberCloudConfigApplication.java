package com.hz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@SpringBootApplication
@EnableConfigServer
public class MemberCloudConfigApplication {

    public static void main(String[] args) {
        SpringApplication.run(MemberCloudConfigApplication.class, args);
    }

}
/*
* http://127.0.0.1:5552/member-cloud-config/dev/master  从github拉去配置文件数据  get请求
* http://127.0.0.1:5552/encrypt/status 测试是否可以加密                           get请求
* http://127.0.0.1:5552/encrypt  参数:application/Json                           post请求
* localhost:5552/bus/refresh      github仓库托管的数据改动 时间消息总线回调接口
* Config-server 将数据同步到Config-client                                        post请求
* */