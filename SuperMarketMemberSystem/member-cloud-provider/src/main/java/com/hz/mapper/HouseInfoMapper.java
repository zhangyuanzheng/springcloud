package com.hz.mapper;


import com.hz.pojo.HouseInfo;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 生产者:dao层连接数据库  接口
 */
@Repository
public interface HouseInfoMapper {

    public  List<HouseInfo> findHouseInfoList();

}
