package com.hz.server;

import com.hz.mapper.HouseInfoMapper;
import com.hz.pojo.HouseInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
public class TestServer {

    @Resource
    private HouseInfoMapper houseInfoMapper;

    /**
     * 测试方法:测试注册中心 生产者和消费者是否连接起来
     * @return
     */
    @RequestMapping(value="loginUser",method = RequestMethod.POST)
    public String loginUser(@RequestParam("username") String username, @RequestParam("userpwd") String  userpwd){

        System.out.println("====服务1");
        return "成功服务1"+username;
    }

    @RequestMapping(value="findHouseInfoList",method = RequestMethod.POST)
    public List<HouseInfo> findHouseInfoList(){
        return houseInfoMapper.findHouseInfoList();
    }

}
