package com.hz;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication(scanBasePackages = {"com.hz"})
@EnableDiscoveryClient //标记为Eureka客户端
@MapperScan("com.hz.mapper")
public class MemberCloudProviderApplication {

    public static void main(String[] args) {
        SpringApplication.run(MemberCloudProviderApplication.class, args);
    }

}
