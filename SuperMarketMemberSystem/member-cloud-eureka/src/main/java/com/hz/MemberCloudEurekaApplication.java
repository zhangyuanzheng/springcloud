package com.hz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer//启动EurekaServer服务端注解
public class MemberCloudEurekaApplication {

    public static void main(String[] args) {
        SpringApplication.run(MemberCloudEurekaApplication.class, args);
    }

}
/*
* http://127.0.0.1:5555/  启动注册中心
* */