package com.hz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;

@SpringBootApplication
@EnableDiscoveryClient  //标记为Eureka客户端
@EnableFeignClients   //Feign(接口调用) HTTP客户端，声明式、模板化，支持Spring MVC注解（其实就是一个controller）
@EnableCircuitBreaker
public class MemberCloudConsumer2Application {

    public static void main(String[] args) {
        SpringApplication.run(MemberCloudConsumer2Application.class, args);
    }

}
