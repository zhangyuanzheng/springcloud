package com.hz.controller;

import com.hz.service.TestService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class TestController {

    @Resource
    private TestService testService;

    @RequestMapping(value = "loginUser", method = RequestMethod.POST)
    public String loginUser(@RequestParam("username") String username, @RequestParam("userpwd") String userpwd) {
        System.out.println("消费者2");
        return testService.loginUser(username, userpwd);
    }
}
