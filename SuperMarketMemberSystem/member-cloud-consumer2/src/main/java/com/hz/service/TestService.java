package com.hz.service;


import com.hz.service.impl.TestServiceImpl;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Component("testService")
@FeignClient(name = "member-cloud-provider", fallback = TestServiceImpl.class)
public interface TestService {

    /**
     * 测试方法:测试注册中心 生产者和消费者是否连接起来
     *
     * @return
     */
    @RequestMapping(value = "loginUser", method = RequestMethod.POST)
    public String loginUser(@RequestParam("username") String username, @RequestParam("userpwd") String userpwd);
}
