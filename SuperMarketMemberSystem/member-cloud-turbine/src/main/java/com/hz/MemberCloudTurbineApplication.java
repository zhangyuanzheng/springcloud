package com.hz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.turbine.EnableTurbine;

@SpringBootApplication
@EnableTurbine //监控多项目注解
@EnableDiscoveryClient //Eureka客户端注解
public class MemberCloudTurbineApplication {

    public static void main(String[] args) {
        SpringApplication.run(MemberCloudTurbineApplication.class, args);
    }

}
