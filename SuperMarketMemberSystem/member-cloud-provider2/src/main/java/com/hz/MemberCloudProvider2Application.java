package com.hz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient //标记为Eureka客户端
public class MemberCloudProvider2Application {

    public static void main(String[] args) {
        SpringApplication.run(MemberCloudProvider2Application.class, args);
    }

}
