package com.hz.server;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestServer {

    /**
     * 测试方法:测试注册中心 生产者和消费者是否连接起来
     * @return
     */
    @RequestMapping(value="loginUser",method = RequestMethod.POST)
    public String loginUser(@RequestParam("username") String username, @RequestParam("userpwd") String  userpwd){

        System.out.println("====服务2");
        return "成功服务2"+username;
    }

}
