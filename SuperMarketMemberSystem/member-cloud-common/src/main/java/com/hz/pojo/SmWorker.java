package com.sample;


public class SmWorker {

  private long workerId;
  private String workerName;
  private String workerDepartName;
  private long workerSex;
  private String workerBlith;
  private String workerEmploy;
  private double workerSal;
  private long workerTel;
  private String workerAddr;
  private long workerState;


  public long getWorkerId() {
    return workerId;
  }

  public void setWorkerId(long workerId) {
    this.workerId = workerId;
  }


  public String getWorkerName() {
    return workerName;
  }

  public void setWorkerName(String workerName) {
    this.workerName = workerName;
  }


  public String getWorkerDepartName() {
    return workerDepartName;
  }

  public void setWorkerDepartName(String workerDepartName) {
    this.workerDepartName = workerDepartName;
  }


  public long getWorkerSex() {
    return workerSex;
  }

  public void setWorkerSex(long workerSex) {
    this.workerSex = workerSex;
  }


  public String getWorkerBlith() {
    return workerBlith;
  }

  public void setWorkerBlith(String workerBlith) {
    this.workerBlith = workerBlith;
  }


  public String getWorkerEmploy() {
    return workerEmploy;
  }

  public void setWorkerEmploy(String workerEmploy) {
    this.workerEmploy = workerEmploy;
  }


  public double getWorkerSal() {
    return workerSal;
  }

  public void setWorkerSal(double workerSal) {
    this.workerSal = workerSal;
  }


  public long getWorkerTel() {
    return workerTel;
  }

  public void setWorkerTel(long workerTel) {
    this.workerTel = workerTel;
  }


  public String getWorkerAddr() {
    return workerAddr;
  }

  public void setWorkerAddr(String workerAddr) {
    this.workerAddr = workerAddr;
  }


  public long getWorkerState() {
    return workerState;
  }

  public void setWorkerState(long workerState) {
    this.workerState = workerState;
  }

}
