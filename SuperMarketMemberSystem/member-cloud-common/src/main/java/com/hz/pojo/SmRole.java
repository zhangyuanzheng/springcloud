package com.sample;


public class SmRole {

  private long roleId;
  private String roleName;
  private String roleRemarks;
  private long roleNumber;


  public long getRoleId() {
    return roleId;
  }

  public void setRoleId(long roleId) {
    this.roleId = roleId;
  }


  public String getRoleName() {
    return roleName;
  }

  public void setRoleName(String roleName) {
    this.roleName = roleName;
  }


  public String getRoleRemarks() {
    return roleRemarks;
  }

  public void setRoleRemarks(String roleRemarks) {
    this.roleRemarks = roleRemarks;
  }


  public long getRoleNumber() {
    return roleNumber;
  }

  public void setRoleNumber(long roleNumber) {
    this.roleNumber = roleNumber;
  }

}
