package com.sample;


public class SmInventory {

  private long inventoryId;
  private long inventoryAdminId;
  private String inventoryTime;
  private String inventoryGoods;
  private long inventoryGoodsCount;
  private long inventoryGoodsStatus;


  public long getInventoryId() {
    return inventoryId;
  }

  public void setInventoryId(long inventoryId) {
    this.inventoryId = inventoryId;
  }


  public long getInventoryAdminId() {
    return inventoryAdminId;
  }

  public void setInventoryAdminId(long inventoryAdminId) {
    this.inventoryAdminId = inventoryAdminId;
  }


  public String getInventoryTime() {
    return inventoryTime;
  }

  public void setInventoryTime(String inventoryTime) {
    this.inventoryTime = inventoryTime;
  }


  public String getInventoryGoods() {
    return inventoryGoods;
  }

  public void setInventoryGoods(String inventoryGoods) {
    this.inventoryGoods = inventoryGoods;
  }


  public long getInventoryGoodsCount() {
    return inventoryGoodsCount;
  }

  public void setInventoryGoodsCount(long inventoryGoodsCount) {
    this.inventoryGoodsCount = inventoryGoodsCount;
  }


  public long getInventoryGoodsStatus() {
    return inventoryGoodsStatus;
  }

  public void setInventoryGoodsStatus(long inventoryGoodsStatus) {
    this.inventoryGoodsStatus = inventoryGoodsStatus;
  }

}
