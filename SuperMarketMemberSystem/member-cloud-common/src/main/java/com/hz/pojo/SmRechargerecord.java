package com.sample;


public class SmRechargerecord {

  private long rechargerecordId;
  private long rechargerecordMemberId;
  private double rechargerecordSummoney;
  private String rechargerecordTime;
  private long rechargerecordType;


  public long getRechargerecordId() {
    return rechargerecordId;
  }

  public void setRechargerecordId(long rechargerecordId) {
    this.rechargerecordId = rechargerecordId;
  }


  public long getRechargerecordMemberId() {
    return rechargerecordMemberId;
  }

  public void setRechargerecordMemberId(long rechargerecordMemberId) {
    this.rechargerecordMemberId = rechargerecordMemberId;
  }


  public double getRechargerecordSummoney() {
    return rechargerecordSummoney;
  }

  public void setRechargerecordSummoney(double rechargerecordSummoney) {
    this.rechargerecordSummoney = rechargerecordSummoney;
  }


  public String getRechargerecordTime() {
    return rechargerecordTime;
  }

  public void setRechargerecordTime(String rechargerecordTime) {
    this.rechargerecordTime = rechargerecordTime;
  }


  public long getRechargerecordType() {
    return rechargerecordType;
  }

  public void setRechargerecordType(long rechargerecordType) {
    this.rechargerecordType = rechargerecordType;
  }

}
