package com.sample;


public class SmRepurchase {

  private long repurchaseId;
  private String repurchaseGoodsName;
  private long repurchaseGoodsCount;
  private String repurchaseGoodsTime;
  private long repurchaseGoodsStatus;
  private long repurchaseAdminId;


  public long getRepurchaseId() {
    return repurchaseId;
  }

  public void setRepurchaseId(long repurchaseId) {
    this.repurchaseId = repurchaseId;
  }


  public String getRepurchaseGoodsName() {
    return repurchaseGoodsName;
  }

  public void setRepurchaseGoodsName(String repurchaseGoodsName) {
    this.repurchaseGoodsName = repurchaseGoodsName;
  }


  public long getRepurchaseGoodsCount() {
    return repurchaseGoodsCount;
  }

  public void setRepurchaseGoodsCount(long repurchaseGoodsCount) {
    this.repurchaseGoodsCount = repurchaseGoodsCount;
  }


  public String getRepurchaseGoodsTime() {
    return repurchaseGoodsTime;
  }

  public void setRepurchaseGoodsTime(String repurchaseGoodsTime) {
    this.repurchaseGoodsTime = repurchaseGoodsTime;
  }


  public long getRepurchaseGoodsStatus() {
    return repurchaseGoodsStatus;
  }

  public void setRepurchaseGoodsStatus(long repurchaseGoodsStatus) {
    this.repurchaseGoodsStatus = repurchaseGoodsStatus;
  }


  public long getRepurchaseAdminId() {
    return repurchaseAdminId;
  }

  public void setRepurchaseAdminId(long repurchaseAdminId) {
    this.repurchaseAdminId = repurchaseAdminId;
  }

}
