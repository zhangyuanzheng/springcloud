package com.sample;


public class SmConsumption {

  private long consumptionId;
  private long consumptionMemberId;
  private double consumptionMoney;
  private String consumptionTime;


  public long getConsumptionId() {
    return consumptionId;
  }

  public void setConsumptionId(long consumptionId) {
    this.consumptionId = consumptionId;
  }


  public long getConsumptionMemberId() {
    return consumptionMemberId;
  }

  public void setConsumptionMemberId(long consumptionMemberId) {
    this.consumptionMemberId = consumptionMemberId;
  }


  public double getConsumptionMoney() {
    return consumptionMoney;
  }

  public void setConsumptionMoney(double consumptionMoney) {
    this.consumptionMoney = consumptionMoney;
  }


  public String getConsumptionTime() {
    return consumptionTime;
  }

  public void setConsumptionTime(String consumptionTime) {
    this.consumptionTime = consumptionTime;
  }

}
