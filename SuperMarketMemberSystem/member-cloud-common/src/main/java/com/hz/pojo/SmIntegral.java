package com.sample;


public class SmIntegral {

  private long integralId;
  private long integralMemberId;
  private long integralIntegralnumbers;
  private String integralTime;


  public long getIntegralId() {
    return integralId;
  }

  public void setIntegralId(long integralId) {
    this.integralId = integralId;
  }


  public long getIntegralMemberId() {
    return integralMemberId;
  }

  public void setIntegralMemberId(long integralMemberId) {
    this.integralMemberId = integralMemberId;
  }


  public long getIntegralIntegralnumbers() {
    return integralIntegralnumbers;
  }

  public void setIntegralIntegralnumbers(long integralIntegralnumbers) {
    this.integralIntegralnumbers = integralIntegralnumbers;
  }


  public String getIntegralTime() {
    return integralTime;
  }

  public void setIntegralTime(String integralTime) {
    this.integralTime = integralTime;
  }

}
