package com.sample;


public class SmMember {

  private long memberId;
  private String memberNumber;
  private String memberName;
  private String memberBirthday;
  private long memberSex;
  private long memberLevelId;


  public long getMemberId() {
    return memberId;
  }

  public void setMemberId(long memberId) {
    this.memberId = memberId;
  }


  public String getMemberNumber() {
    return memberNumber;
  }

  public void setMemberNumber(String memberNumber) {
    this.memberNumber = memberNumber;
  }


  public String getMemberName() {
    return memberName;
  }

  public void setMemberName(String memberName) {
    this.memberName = memberName;
  }


  public String getMemberBirthday() {
    return memberBirthday;
  }

  public void setMemberBirthday(String memberBirthday) {
    this.memberBirthday = memberBirthday;
  }


  public long getMemberSex() {
    return memberSex;
  }

  public void setMemberSex(long memberSex) {
    this.memberSex = memberSex;
  }


  public long getMemberLevelId() {
    return memberLevelId;
  }

  public void setMemberLevelId(long memberLevelId) {
    this.memberLevelId = memberLevelId;
  }

}
