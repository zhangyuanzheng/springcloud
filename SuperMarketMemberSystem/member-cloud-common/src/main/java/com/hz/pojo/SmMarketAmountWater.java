package com.sample;


public class SmMarketAmountWater {

  private long marketAmountWaterId;
  private String marketAmountWaterGoodsName;
  private double marketAmountWaterOnePrice;
  private double marketAmountWaterAllPrice;


  public long getMarketAmountWaterId() {
    return marketAmountWaterId;
  }

  public void setMarketAmountWaterId(long marketAmountWaterId) {
    this.marketAmountWaterId = marketAmountWaterId;
  }


  public String getMarketAmountWaterGoodsName() {
    return marketAmountWaterGoodsName;
  }

  public void setMarketAmountWaterGoodsName(String marketAmountWaterGoodsName) {
    this.marketAmountWaterGoodsName = marketAmountWaterGoodsName;
  }


  public double getMarketAmountWaterOnePrice() {
    return marketAmountWaterOnePrice;
  }

  public void setMarketAmountWaterOnePrice(double marketAmountWaterOnePrice) {
    this.marketAmountWaterOnePrice = marketAmountWaterOnePrice;
  }


  public double getMarketAmountWaterAllPrice() {
    return marketAmountWaterAllPrice;
  }

  public void setMarketAmountWaterAllPrice(double marketAmountWaterAllPrice) {
    this.marketAmountWaterAllPrice = marketAmountWaterAllPrice;
  }

}
