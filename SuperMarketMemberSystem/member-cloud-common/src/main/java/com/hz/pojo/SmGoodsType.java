package com.sample;


public class SmGoodsType {

  private long goodsTypeId;
  private String goodsTypeName;


  public long getGoodsTypeId() {
    return goodsTypeId;
  }

  public void setGoodsTypeId(long goodsTypeId) {
    this.goodsTypeId = goodsTypeId;
  }


  public String getGoodsTypeName() {
    return goodsTypeName;
  }

  public void setGoodsTypeName(String goodsTypeName) {
    this.goodsTypeName = goodsTypeName;
  }

}
