package com.hz.pojo;


public class SmAdmin {

  private long adminId;
  private String adminName;
  private String adminPwd;
  private long adminRoleId;
  private String adminAddtime;
  private String adminLoginEndtime;
  private long adminLoginNumber;
  private long adminState;


  public long getAdminId() {
    return adminId;
  }

  public void setAdminId(long adminId) {
    this.adminId = adminId;
  }


  public String getAdminName() {
    return adminName;
  }

  public void setAdminName(String adminName) {
    this.adminName = adminName;
  }


  public String getAdminPwd() {
    return adminPwd;
  }

  public void setAdminPwd(String adminPwd) {
    this.adminPwd = adminPwd;
  }


  public long getAdminRoleId() {
    return adminRoleId;
  }

  public void setAdminRoleId(long adminRoleId) {
    this.adminRoleId = adminRoleId;
  }


  public String getAdminAddtime() {
    return adminAddtime;
  }

  public void setAdminAddtime(String adminAddtime) {
    this.adminAddtime = adminAddtime;
  }


  public String getAdminLoginEndtime() {
    return adminLoginEndtime;
  }

  public void setAdminLoginEndtime(String adminLoginEndtime) {
    this.adminLoginEndtime = adminLoginEndtime;
  }


  public long getAdminLoginNumber() {
    return adminLoginNumber;
  }

  public void setAdminLoginNumber(long adminLoginNumber) {
    this.adminLoginNumber = adminLoginNumber;
  }


  public long getAdminState() {
    return adminState;
  }

  public void setAdminState(long adminState) {
    this.adminState = adminState;
  }

}
