package com.sample;


public class SmGoods {

  private long goodsId;
  private String goodsName;
  private double goodsPrice;
  private long goodsNumber;
  private String goodsDate;
  private String goodsAddr;
  private String goodsType;
  private long goodsState;


  public long getGoodsId() {
    return goodsId;
  }

  public void setGoodsId(long goodsId) {
    this.goodsId = goodsId;
  }


  public String getGoodsName() {
    return goodsName;
  }

  public void setGoodsName(String goodsName) {
    this.goodsName = goodsName;
  }


  public double getGoodsPrice() {
    return goodsPrice;
  }

  public void setGoodsPrice(double goodsPrice) {
    this.goodsPrice = goodsPrice;
  }


  public long getGoodsNumber() {
    return goodsNumber;
  }

  public void setGoodsNumber(long goodsNumber) {
    this.goodsNumber = goodsNumber;
  }


  public String getGoodsDate() {
    return goodsDate;
  }

  public void setGoodsDate(String goodsDate) {
    this.goodsDate = goodsDate;
  }


  public String getGoodsAddr() {
    return goodsAddr;
  }

  public void setGoodsAddr(String goodsAddr) {
    this.goodsAddr = goodsAddr;
  }


  public String getGoodsType() {
    return goodsType;
  }

  public void setGoodsType(String goodsType) {
    this.goodsType = goodsType;
  }


  public long getGoodsState() {
    return goodsState;
  }

  public void setGoodsState(long goodsState) {
    this.goodsState = goodsState;
  }

}
