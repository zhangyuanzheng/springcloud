package com.hz.pojo;


public class SmAcount {

  private long accountId;
  private long accountMemberId;
  private double accountMoney;
  private double accountSummoney;
  private String accountAddtime;


  public long getAccountId() {
    return accountId;
  }

  public void setAccountId(long accountId) {
    this.accountId = accountId;
  }


  public long getAccountMemberId() {
    return accountMemberId;
  }

  public void setAccountMemberId(long accountMemberId) {
    this.accountMemberId = accountMemberId;
  }


  public double getAccountMoney() {
    return accountMoney;
  }

  public void setAccountMoney(double accountMoney) {
    this.accountMoney = accountMoney;
  }


  public double getAccountSummoney() {
    return accountSummoney;
  }

  public void setAccountSummoney(double accountSummoney) {
    this.accountSummoney = accountSummoney;
  }


  public String getAccountAddtime() {
    return accountAddtime;
  }

  public void setAccountAddtime(String accountAddtime) {
    this.accountAddtime = accountAddtime;
  }

}
