package com.sample;


public class SmSupplier {

  private long supplierId;
  private String supplierDesc;
  private String supplierAddress;
  private long supplierAdminId;
  private String supplierTime;
  private String supplierName;
  private String supplierPeople;


  public long getSupplierId() {
    return supplierId;
  }

  public void setSupplierId(long supplierId) {
    this.supplierId = supplierId;
  }


  public String getSupplierDesc() {
    return supplierDesc;
  }

  public void setSupplierDesc(String supplierDesc) {
    this.supplierDesc = supplierDesc;
  }


  public String getSupplierAddress() {
    return supplierAddress;
  }

  public void setSupplierAddress(String supplierAddress) {
    this.supplierAddress = supplierAddress;
  }


  public long getSupplierAdminId() {
    return supplierAdminId;
  }

  public void setSupplierAdminId(long supplierAdminId) {
    this.supplierAdminId = supplierAdminId;
  }


  public String getSupplierTime() {
    return supplierTime;
  }

  public void setSupplierTime(String supplierTime) {
    this.supplierTime = supplierTime;
  }


  public String getSupplierName() {
    return supplierName;
  }

  public void setSupplierName(String supplierName) {
    this.supplierName = supplierName;
  }


  public String getSupplierPeople() {
    return supplierPeople;
  }

  public void setSupplierPeople(String supplierPeople) {
    this.supplierPeople = supplierPeople;
  }

}
