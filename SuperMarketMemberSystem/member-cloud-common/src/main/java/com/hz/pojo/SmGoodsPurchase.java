package com.sample;


public class SmGoodsPurchase {

  private long goodsPurchaseId;
  private String goodsPurchaseName;
  private long goodsPurchaseOneNumber;
  private long goodsPurchaseAllNumber;
  private long goodsPurchaseWorkerId;
  private String goodsPurchaseWorkerName;


  public long getGoodsPurchaseId() {
    return goodsPurchaseId;
  }

  public void setGoodsPurchaseId(long goodsPurchaseId) {
    this.goodsPurchaseId = goodsPurchaseId;
  }


  public String getGoodsPurchaseName() {
    return goodsPurchaseName;
  }

  public void setGoodsPurchaseName(String goodsPurchaseName) {
    this.goodsPurchaseName = goodsPurchaseName;
  }


  public long getGoodsPurchaseOneNumber() {
    return goodsPurchaseOneNumber;
  }

  public void setGoodsPurchaseOneNumber(long goodsPurchaseOneNumber) {
    this.goodsPurchaseOneNumber = goodsPurchaseOneNumber;
  }


  public long getGoodsPurchaseAllNumber() {
    return goodsPurchaseAllNumber;
  }

  public void setGoodsPurchaseAllNumber(long goodsPurchaseAllNumber) {
    this.goodsPurchaseAllNumber = goodsPurchaseAllNumber;
  }


  public long getGoodsPurchaseWorkerId() {
    return goodsPurchaseWorkerId;
  }

  public void setGoodsPurchaseWorkerId(long goodsPurchaseWorkerId) {
    this.goodsPurchaseWorkerId = goodsPurchaseWorkerId;
  }


  public String getGoodsPurchaseWorkerName() {
    return goodsPurchaseWorkerName;
  }

  public void setGoodsPurchaseWorkerName(String goodsPurchaseWorkerName) {
    this.goodsPurchaseWorkerName = goodsPurchaseWorkerName;
  }

}
