package com.sample;


public class SmPurchase {

  private long purchaseId;
  private String purchaseGoodsName;
  private String purchaseGoodsDesc;
  private long purchaseGoodsCount;
  private double purchaseGoodsTotal;
  private long purchaseGoodsStatus;
  private long purchaseAdminId;
  private String purchaseCreateTime;
  private long purchaseSupplierId;


  public long getPurchaseId() {
    return purchaseId;
  }

  public void setPurchaseId(long purchaseId) {
    this.purchaseId = purchaseId;
  }


  public String getPurchaseGoodsName() {
    return purchaseGoodsName;
  }

  public void setPurchaseGoodsName(String purchaseGoodsName) {
    this.purchaseGoodsName = purchaseGoodsName;
  }


  public String getPurchaseGoodsDesc() {
    return purchaseGoodsDesc;
  }

  public void setPurchaseGoodsDesc(String purchaseGoodsDesc) {
    this.purchaseGoodsDesc = purchaseGoodsDesc;
  }


  public long getPurchaseGoodsCount() {
    return purchaseGoodsCount;
  }

  public void setPurchaseGoodsCount(long purchaseGoodsCount) {
    this.purchaseGoodsCount = purchaseGoodsCount;
  }


  public double getPurchaseGoodsTotal() {
    return purchaseGoodsTotal;
  }

  public void setPurchaseGoodsTotal(double purchaseGoodsTotal) {
    this.purchaseGoodsTotal = purchaseGoodsTotal;
  }


  public long getPurchaseGoodsStatus() {
    return purchaseGoodsStatus;
  }

  public void setPurchaseGoodsStatus(long purchaseGoodsStatus) {
    this.purchaseGoodsStatus = purchaseGoodsStatus;
  }


  public long getPurchaseAdminId() {
    return purchaseAdminId;
  }

  public void setPurchaseAdminId(long purchaseAdminId) {
    this.purchaseAdminId = purchaseAdminId;
  }


  public String getPurchaseCreateTime() {
    return purchaseCreateTime;
  }

  public void setPurchaseCreateTime(String purchaseCreateTime) {
    this.purchaseCreateTime = purchaseCreateTime;
  }


  public long getPurchaseSupplierId() {
    return purchaseSupplierId;
  }

  public void setPurchaseSupplierId(long purchaseSupplierId) {
    this.purchaseSupplierId = purchaseSupplierId;
  }

}
