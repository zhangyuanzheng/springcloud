package com.sample;


public class SmGoodsSold {

  private long goodsSoldId;
  private String goodsSoldName;
  private long goodsSoldNumber;
  private double goodsSoldOnePrice;
  private double goodsSoldAmountPrice;
  private String goodsSoldTime;
  private long goodsSoldMemberId;
  private double goodsSoldAllPrice;


  public long getGoodsSoldId() {
    return goodsSoldId;
  }

  public void setGoodsSoldId(long goodsSoldId) {
    this.goodsSoldId = goodsSoldId;
  }


  public String getGoodsSoldName() {
    return goodsSoldName;
  }

  public void setGoodsSoldName(String goodsSoldName) {
    this.goodsSoldName = goodsSoldName;
  }


  public long getGoodsSoldNumber() {
    return goodsSoldNumber;
  }

  public void setGoodsSoldNumber(long goodsSoldNumber) {
    this.goodsSoldNumber = goodsSoldNumber;
  }


  public double getGoodsSoldOnePrice() {
    return goodsSoldOnePrice;
  }

  public void setGoodsSoldOnePrice(double goodsSoldOnePrice) {
    this.goodsSoldOnePrice = goodsSoldOnePrice;
  }


  public double getGoodsSoldAmountPrice() {
    return goodsSoldAmountPrice;
  }

  public void setGoodsSoldAmountPrice(double goodsSoldAmountPrice) {
    this.goodsSoldAmountPrice = goodsSoldAmountPrice;
  }


  public String getGoodsSoldTime() {
    return goodsSoldTime;
  }

  public void setGoodsSoldTime(String goodsSoldTime) {
    this.goodsSoldTime = goodsSoldTime;
  }


  public long getGoodsSoldMemberId() {
    return goodsSoldMemberId;
  }

  public void setGoodsSoldMemberId(long goodsSoldMemberId) {
    this.goodsSoldMemberId = goodsSoldMemberId;
  }


  public double getGoodsSoldAllPrice() {
    return goodsSoldAllPrice;
  }

  public void setGoodsSoldAllPrice(double goodsSoldAllPrice) {
    this.goodsSoldAllPrice = goodsSoldAllPrice;
  }

}
