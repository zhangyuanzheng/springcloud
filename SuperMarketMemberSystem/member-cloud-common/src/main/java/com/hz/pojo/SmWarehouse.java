package com.sample;


public class SmWarehouse {

  private long warehouseId;
  private String warehouseName;
  private long warehouseAdminId;
  private String warehousePhone;
  private String warehouseAddress;
  private long warehouseStatus;


  public long getWarehouseId() {
    return warehouseId;
  }

  public void setWarehouseId(long warehouseId) {
    this.warehouseId = warehouseId;
  }


  public String getWarehouseName() {
    return warehouseName;
  }

  public void setWarehouseName(String warehouseName) {
    this.warehouseName = warehouseName;
  }


  public long getWarehouseAdminId() {
    return warehouseAdminId;
  }

  public void setWarehouseAdminId(long warehouseAdminId) {
    this.warehouseAdminId = warehouseAdminId;
  }


  public String getWarehousePhone() {
    return warehousePhone;
  }

  public void setWarehousePhone(String warehousePhone) {
    this.warehousePhone = warehousePhone;
  }


  public String getWarehouseAddress() {
    return warehouseAddress;
  }

  public void setWarehouseAddress(String warehouseAddress) {
    this.warehouseAddress = warehouseAddress;
  }


  public long getWarehouseStatus() {
    return warehouseStatus;
  }

  public void setWarehouseStatus(long warehouseStatus) {
    this.warehouseStatus = warehouseStatus;
  }

}
