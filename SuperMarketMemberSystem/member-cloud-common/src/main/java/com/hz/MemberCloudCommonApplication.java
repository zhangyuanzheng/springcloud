package com.hz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MemberCloudCommonApplication {

    public static void main(String[] args) {
        SpringApplication.run(MemberCloudCommonApplication.class, args);
    }

}
